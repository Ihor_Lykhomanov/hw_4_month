﻿using NUnitTestProject1.Support;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facebook.POM
{
    internal static class TempMailPOM
    {   
        internal static By _tempEmail = By.CssSelector("div.temp-mail__field.temp-mail--container-shadow > div");
        internal static By _updateEmail = By.CssSelector(" div.temp-mail__field.buttons-wrapper > a.temp-mail__button.button.button--remove");
        internal static string GetTempEmail() => ChromeBrowser.GetDriver().FindElement(_tempEmail).Text;
        internal static void UpdateEmail() => ChromeBrowser.GetDriver().FindElement(_updateEmail).Click();
    }
}
