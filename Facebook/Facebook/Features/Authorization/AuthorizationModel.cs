﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facebook.Features.Authorization
{
    class AuthorizationModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Notification { get; set; }
        public string UserName { get; set; }
        public string UserLastName { get; set; }
    }
}
