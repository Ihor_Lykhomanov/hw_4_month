﻿@HW_Friday/29.10.2021 @RegistrationModule
Feature: Registration
    In order to log in application of Facebook
	As a user of application 
	I want to be able to registrate in application

Background:
	Given Registration page is open

@p1 @smoke @PositiveScenario
Scenario Outline: Registration with valid data
	Given The data is filled in the registration field
		| FirstName    | LastName    | NewPassword    |
		| <first name> | <last name> | <new password> |
	When I choose in field birthday
		| Month   | Day   | Year   |
		| <month> | <day> | <year> |
	And I choose 'Gender'
		| Gender   |
		| <gender> |
	And I press the button Sign up to register
	And I enter the confirmation code that came to my phone
	Then  Account created
	And Open main page of application
Examples:
		| first name | last name | new password       | month | day | year | gender |
		| Anton      | Chernev   | DTEK_Chernev20     | Nov   | 5   | 1995 | Male   |
		| Anastasia  | Bilenko   | Qwert12345         | Jan   | 1   | 1905 | Female |
		| Ray        | Parker    | qwert.qwert.qwert. | Sep   | 31  | 2016 | Male   |

@p1 @smoke @CustomGender
Scenario Outline: Registration with valid data for Custom gender
	Given The data is filled in the registration field
		| FirstName    | LastName    | New password   |
		| <first name> | <last name> | <new password> |
	When I choose in field birthday
		| Month   | Day   | Year   |
		| <month> | <day> | <year> |
	And I choose 'Gender'
		| Gender   |
		| <gender> |
	And I choose '<pronoun>' in field Select your pronoun
	And I enter the 'Iron man' into the field Gender(optional)
	And I press the button Sign up to register
	And I enter the confirmation code that came to my phone
	Then Account created
	And Open main page of application
Examples:
		| first name | last name | new password       | month | day | year | pronoun                             |
		| Anton      | Chernev   | DTEK_Chernev20     | Nov   | 5   | 2011 | He:"Whish him a happy birthday!"    |
		| Anastasia  | Bilenko   | Qwert12345         | Jan   | 1   | 1972 | She:"Whish her a happy birthday!"   |
		| Ray        | Parker    | qwert.qwert.qwert. | Sep   | 31  | 2016 | They:"Whish them a happy birthday!" |

@p1 @NegativeScenario
Scenario Outline: Registration with not valid data
	Given The data is filled in the registration field
		| FirstName    | LastName    | MobileNumberOrEmail      | NewPassword    |
		| <first name> | <last name> | <mobile number or email> | <new password> |
	When I choose in field birthday
		| Month   | Day   | Year   |
		| <month> | <day> | <year> |
	And I choose 'Gender'
		| Gender   |
		| <gender> |
	And I press the button Sign up to register
	Then The Notification is displayed
		| Notification   |
		| <notification> |
Examples:
		| first name | last name | mobile number or email | new password | month | day | year | gender | notification                                                                              |
		| /          | Bilenko   | nestea17@gmail.com     | Qwert12345   | Jan   | 1   | 1905 | Female | This name has certain characters that aren't allowed. Learn more about our name policies. |
		| Anastasia  | .         | nestea17@gmail.com     | Qwert12345   | Jan   | 1   | 1905 | Female | This name has certain characters that aren't allowed. Learn more about our name policies. |
		| 4          | Bilenko   | nestea17@gmail.com     | Qwert12345   | Jan   | 1   | 1905 | Female | This name has certain characters that aren't allowed. Learn more about our name policies. |
		| Anastasia  | Bilenko   |                        | Qwert12345   | Jan   | 1   | 1905 | Female | You'll use this when you log ib and if you ever need to reset your password.              |
		| Anastasia  | 1         | nestea17@gmail.com     | Qwert12345   | Jan   | 1   | 1905 | Female | This name has certain characters that aren't allowed. Learn more about our name policies. |
		| Anastasia  | Bilenko   | nestea17gmail.com      | Qwert12345   | Jan   | 1   | 1905 | Female | Please enter a valid mobile or email address.                                             |
		| Anastasia  | Bilenko   | nestea17gmail.com      | Qwert12345   | Jan   | 1   | 1905 |        | Please choose a gender. You can change who can see this later.                            |
		| Anastasia  | Bilenko   | nestea17@gmail.com     | Qwert12345   | Jan   | 1   | 1905 | Female | Please enter your age.                                                                    |
		| Anastasia  | Bilenko   | nestea17@gmail.com     |              | Jan   | 1   | 1905 | Female |                                                                                           |

#@p1 @PositiveScenario
#Scenario: Password recovery
#	When I click the button 'Forgot Password?'
#	And I click the button 'Continue'
#	And I enter the security code that came to my phone
#	And I am entering a new password
#	And I click the button 'Continue'
#	Then The password was changed

#@p2 @NegativeScenario
#Scenario Outline: Registration with an existing account
#	Given The data is filled in the registration field
#		| First name   | Last name   | Mobile number or email   | New password   |
#		| <first name> | <last name> | <mobile number or email> | <new password> |
#	When I choose in field birthday
#		| Month   | Day   | Year   |
#		| <month> | <day> | <year> |
#	And I choose 'Gender'
#	And I press the button Sign up to register
#	And I enter the confirmation code that came to my phone
#	Then The main page of an existing account has opened
#Examples:
#		| first name | last name | mobile number or email | new password | month | day | year | gender |
#		| Иван       | Субботин  | +380981019893          | yansubb.2001 | Oct   | 12  | 2001 | Male   |
#		| Anastasia  | Bilenko   | nestea17@gmail.com     | Qwert12345   | Jan   | 1   | 1905 | Female |