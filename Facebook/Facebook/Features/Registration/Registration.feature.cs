﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Facebook.Features.Registration
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Registration")]
    [NUnit.Framework.CategoryAttribute("HW_Friday/29.10.2021")]
    [NUnit.Framework.CategoryAttribute("RegistrationModule")]
    public partial class RegistrationFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "Registration.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Registration", "    In order to log in application of Facebook\r\n\tAs a user of application \r\n\tI wa" +
                    "nt to be able to registrate in application", ProgrammingLanguage.CSharp, new string[] {
                        "HW_Friday/29.10.2021",
                        "RegistrationModule"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 7
#line 8
 testRunner.Given("Registration page is open", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Registration with valid data")]
        [NUnit.Framework.CategoryAttribute("p1")]
        [NUnit.Framework.CategoryAttribute("smoke")]
        [NUnit.Framework.CategoryAttribute("PositiveScenario")]
        [NUnit.Framework.TestCaseAttribute("Anton", "Chernev", "DTEK_Chernev20", "Nov", "5", "1995", "Male", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", "Bilenko", "Qwert12345", "Jan", "1", "1905", "Female", null)]
        [NUnit.Framework.TestCaseAttribute("Ray", "Parker", "qwert.qwert.qwert.", "Sep", "31", "2016", "Male", null)]
        public virtual void RegistrationWithValidData(string firstName, string lastName, string newPassword, string month, string day, string year, string gender, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "p1",
                    "smoke",
                    "PositiveScenario"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Registration with valid data", null, @__tags);
#line 11
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 7
this.FeatureBackground();
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "FirstName",
                        "LastName",
                        "NewPassword"});
            table1.AddRow(new string[] {
                        string.Format("{0}", firstName),
                        string.Format("{0}", lastName),
                        string.Format("{0}", newPassword)});
#line 12
 testRunner.Given("The data is filled in the registration field", ((string)(null)), table1, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "Month",
                        "Day",
                        "Year"});
            table2.AddRow(new string[] {
                        string.Format("{0}", month),
                        string.Format("{0}", day),
                        string.Format("{0}", year)});
#line 15
 testRunner.When("I choose in field birthday", ((string)(null)), table2, "When ");
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "Gender"});
            table3.AddRow(new string[] {
                        string.Format("{0}", gender)});
#line 18
 testRunner.And("I choose \'Gender\'", ((string)(null)), table3, "And ");
#line 21
 testRunner.And("I press the button Sign up to register", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 22
 testRunner.And("I enter the confirmation code that came to my phone", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 23
 testRunner.Then("Account created", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 24
 testRunner.And("Open main page of application", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Registration with valid data for Custom gender")]
        [NUnit.Framework.CategoryAttribute("p1")]
        [NUnit.Framework.CategoryAttribute("smoke")]
        [NUnit.Framework.CategoryAttribute("CustomGender")]
        [NUnit.Framework.TestCaseAttribute("Anton", "Chernev", "DTEK_Chernev20", "Nov", "5", "2011", "He:\"Whish him a happy birthday!\"", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", "Bilenko", "Qwert12345", "Jan", "1", "1972", "She:\"Whish her a happy birthday!\"", null)]
        [NUnit.Framework.TestCaseAttribute("Ray", "Parker", "qwert.qwert.qwert.", "Sep", "31", "2016", "They:\"Whish them a happy birthday!\"", null)]
        public virtual void RegistrationWithValidDataForCustomGender(string firstName, string lastName, string newPassword, string month, string day, string year, string pronoun, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "p1",
                    "smoke",
                    "CustomGender"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Registration with valid data for Custom gender", null, @__tags);
#line 32
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 7
this.FeatureBackground();
#line hidden
            TechTalk.SpecFlow.Table table4 = new TechTalk.SpecFlow.Table(new string[] {
                        "FirstName",
                        "LastName",
                        "New password"});
            table4.AddRow(new string[] {
                        string.Format("{0}", firstName),
                        string.Format("{0}", lastName),
                        string.Format("{0}", newPassword)});
#line 33
 testRunner.Given("The data is filled in the registration field", ((string)(null)), table4, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table5 = new TechTalk.SpecFlow.Table(new string[] {
                        "Month",
                        "Day",
                        "Year"});
            table5.AddRow(new string[] {
                        string.Format("{0}", month),
                        string.Format("{0}", day),
                        string.Format("{0}", year)});
#line 36
 testRunner.When("I choose in field birthday", ((string)(null)), table5, "When ");
#line hidden
            TechTalk.SpecFlow.Table table6 = new TechTalk.SpecFlow.Table(new string[] {
                        "Gender"});
            table6.AddRow(new string[] {
                        "<gender>"});
#line 39
 testRunner.And("I choose \'Gender\'", ((string)(null)), table6, "And ");
#line 42
 testRunner.And(string.Format("I choose \'{0}\' in field Select your pronoun", pronoun), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 43
 testRunner.And("I enter the \'Iron man\' into the field Gender(optional)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 44
 testRunner.And("I press the button Sign up to register", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 45
 testRunner.And("I enter the confirmation code that came to my phone", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 46
 testRunner.Then("Account created", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 47
 testRunner.And("Open main page of application", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Registration with not valid data")]
        [NUnit.Framework.CategoryAttribute("p1")]
        [NUnit.Framework.CategoryAttribute("NegativeScenario")]
        [NUnit.Framework.TestCaseAttribute("/", "Bilenko", "nestea17@gmail.com", "Qwert12345", "Jan", "1", "1905", "Female", "This name has certain characters that aren\'t allowed. Learn more about our name p" +
            "olicies.", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", ".", "nestea17@gmail.com", "Qwert12345", "Jan", "1", "1905", "Female", "This name has certain characters that aren\'t allowed. Learn more about our name p" +
            "olicies.", null)]
        [NUnit.Framework.TestCaseAttribute("4", "Bilenko", "nestea17@gmail.com", "Qwert12345", "Jan", "1", "1905", "Female", "This name has certain characters that aren\'t allowed. Learn more about our name p" +
            "olicies.", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", "Bilenko", "", "Qwert12345", "Jan", "1", "1905", "Female", "You\'ll use this when you log ib and if you ever need to reset your password.", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", "1", "nestea17@gmail.com", "Qwert12345", "Jan", "1", "1905", "Female", "This name has certain characters that aren\'t allowed. Learn more about our name p" +
            "olicies.", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", "Bilenko", "nestea17gmail.com", "Qwert12345", "Jan", "1", "1905", "Female", "Please enter a valid mobile or email address.", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", "Bilenko", "nestea17gmail.com", "Qwert12345", "Jan", "1", "1905", "", "Please choose a gender. You can change who can see this later.", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", "Bilenko", "nestea17@gmail.com", "Qwert12345", "Jan", "1", "1905", "Female", "Please enter your age.", null)]
        [NUnit.Framework.TestCaseAttribute("Anastasia", "Bilenko", "nestea17@gmail.com", "", "Jan", "1", "1905", "Female", "", null)]
        public virtual void RegistrationWithNotValidData(string firstName, string lastName, string mobileNumberOrEmail, string newPassword, string month, string day, string year, string gender, string notification, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "p1",
                    "NegativeScenario"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Registration with not valid data", null, @__tags);
#line 55
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 7
this.FeatureBackground();
#line hidden
            TechTalk.SpecFlow.Table table7 = new TechTalk.SpecFlow.Table(new string[] {
                        "FirstName",
                        "LastName",
                        "MobileNumberOrEmail",
                        "NewPassword"});
            table7.AddRow(new string[] {
                        string.Format("{0}", firstName),
                        string.Format("{0}", lastName),
                        string.Format("{0}", mobileNumberOrEmail),
                        string.Format("{0}", newPassword)});
#line 56
 testRunner.Given("The data is filled in the registration field", ((string)(null)), table7, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table8 = new TechTalk.SpecFlow.Table(new string[] {
                        "Month",
                        "Day",
                        "Year"});
            table8.AddRow(new string[] {
                        string.Format("{0}", month),
                        string.Format("{0}", day),
                        string.Format("{0}", year)});
#line 59
 testRunner.When("I choose in field birthday", ((string)(null)), table8, "When ");
#line hidden
            TechTalk.SpecFlow.Table table9 = new TechTalk.SpecFlow.Table(new string[] {
                        "Gender"});
            table9.AddRow(new string[] {
                        string.Format("{0}", gender)});
#line 62
 testRunner.And("I choose \'Gender\'", ((string)(null)), table9, "And ");
#line 65
 testRunner.And("I press the button Sign up to register", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table10 = new TechTalk.SpecFlow.Table(new string[] {
                        "Notification"});
            table10.AddRow(new string[] {
                        string.Format("{0}", notification)});
#line 66
 testRunner.Then("The Notification is displayed", ((string)(null)), table10, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

