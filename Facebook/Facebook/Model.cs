﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facebook
{

    public class Rootobject
    {
        public Datum[] data { get; set; }
        public Links links { get; set; }
        public Meta meta { get; set; }
    }

    public class Links
    {
        public string first { get; set; }
        public string last { get; set; }
        public object prev { get; set; }
        public object next { get; set; }
    }

    public class Meta
    {
        public int current_page { get; set; }
        public int from { get; set; }
        public int last_page { get; set; }
        public string path { get; set; }
        public int per_page { get; set; }
        public int to { get; set; }
        public int total { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public Sender sender { get; set; }
        public Inbox inbox { get; set; }
        public string subject { get; set; }
        public DateTime created_at { get; set; }
        public object read { get; set; }
        public bool favorite { get; set; }
        public bool has_html { get; set; }
        public bool has_text { get; set; }
        public int size_in_bytes { get; set; }
        public object[] attachments { get; set; }
    }

    public class Sender
    {
        public string id { get; set; }
        public string display_name { get; set; }
        public string email { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class Inbox
    {
        public string id { get; set; }
        public string display_name { get; set; }
        public string email { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}
