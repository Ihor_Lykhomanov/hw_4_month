﻿using NUnitTestOnFramework.POM.Locators;
using NUnitTestOnFramework.Support;

namespace NUnitTestOnFramework.POM.Methods
{
    internal static class MainPage
    {
        internal static void ClickLoginBtn() =>
            ChromeBrowser.GetDriver().FindElement(MainPageL.LoginBtn).Click();
    }
}