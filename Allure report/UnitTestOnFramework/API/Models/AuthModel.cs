﻿using System;

namespace NUnitTestOnFramework.API.Models
{
    public class AuthModel
    {
        public Token_Data Token_Data { get; set; }
    }

    public class Token_Data
    {
        public string token { get; set; }
        public DateTime token_refresh_expires { get; set; }
        public string firebase_token { get; set; }
        public DateTime firebase_token_expires { get; set; }
    }
}
