﻿namespace NUnitTestOnFramework
{
    public class SelfModel
    {
        public Client_Profile Client_Profile { get; set; }
    }

    public class Client_Profile
    {
        public string id { get; set; }
        public object facebook_followers { get; set; }
        public object instagram_followers { get; set; }
        public bool has_invite { get; set; }
        public string company_website { get; set; }
        public string company_name { get; set; }
        public object company_description { get; set; }
        public object referral { get; set; }
        public string phone_number { get; set; }
        public bool is_sms_enabled { get; set; }
        public float location_latitude { get; set; }
        public float location_longitude { get; set; }
        public string location_name { get; set; }
        public string location_city_name { get; set; }
        public string location_admin1_code { get; set; }
        public string location_timezone { get; set; }
        public string company_address { get; set; }
        public string industry { get; set; }
        public object twitter_followers { get; set; }
        public object youtube_followers { get; set; }
    }

}
