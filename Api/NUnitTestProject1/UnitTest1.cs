using NUnit.Framework;
using NUnitTestProject1.API.Requests;
using NUnitTestProject1.POM.Methods;
using NUnitTestProject1.Support;
using System.Collections.Generic;
using System.Threading;

namespace NUnitTestProject1
{
    public class Tests : Hooks
    {
        [Test]
        public void PasswordRequired()
        {
            MainPage.ClickLoginBtn();
            LoginPage.ClickLoginWithEpicBtn();
            LoginPage.ClickPassword();
            LoginPage.ClickEmail();
            var actualError = LoginPage.GetPasswordError();

            Assert.AreEqual("�����������", actualError);
        }

        [TestCase("tomate8886@omibrown.com", "C/9;<*eUMSUTDR+")]
        public void SuccessLogin(string email, string password)
        {
            MainPage.ClickLoginBtn();
            LoginPage.ClickLoginWithEpicBtn();
            LoginPage.SetEmail(email);
            LoginPage.SetPassword(password);
            LoginPage.ClickEnterBtn();

            Thread.Sleep(5000);
        }

        [TestCase("nkocura3@gmail.com", "Nata_1234", "ygiosdfhasd")]
        public void SuccessLoginApi(string email, string password, string industry)
        {
            var parameters = new Dictionary<string, object>
            {
                {"industry",industry},
                {"location_name","2343 S Throop St, Chicago, IL 60608, USA"},
                {"location_latitude", "41.8494987"},
                {"location_longitude","-87.6582469"},
                {"location_city_name","Chicago"},
                {"location_admin1_code","IL"},
                {"location_timezone","America/Chicago"}
            };

            var token = Auth.AuthPost(email, password).Token_Data.token;
            Profile.ProfilePatch(token, parameters);
            var actIndustry = Self.SelfGet(token).Client_Profile.industry;

            Assert.AreEqual(industry, actIndustry);
        }
        [TestCase("nkocura3@gmail.com", "Nata_1234", "NATASHAAA")]
        public void SuccessUserFirstNameApi(string email, string password, string firstName)
        {
            var parameters = new Dictionary<string, object>
            {
                {"first_name",firstName},
                {"last_name", "Kotsyuraa"}
            };

            var token = Auth.AuthPost(email, password).Token_Data.token;
            Profile.ProfilePatch(token, parameters);
            var temp = Self.GetName(token);
            var actualFirstName = temp.first_name;

            Assert.AreEqual(firstName, actualFirstName);
        }
        [TestCase("nkocura3@gmail.com", "Nata_1234", "COOOSDFDSF")]
        public void SuccessUserLastNameApi(string email, string password, string lastName)
        {
            var parameters = new Dictionary<string, object>
            {
                {"first_name", "dgiyfsaf"},
                {"last_name",lastName}
            };

            var token = Auth.AuthPost(email, password).Token_Data.token;
            Profile.ProfilePatch(token, parameters);
            var actualFirstName = Self.GetName(token).last_name;

            Assert.AreEqual(lastName, actualFirstName);
        }
        [TestCase("cevoni6781@settags.com", "n49bpxeCYG$A!Lb1", "2222222222")]
        public void ChangePhoneNumberApi(string email, string password, string phone)
        {
            var parameters = new Dictionary<string, object>
            {
                {"password", "n49bpxeCYG$A!Lb1"},
                {"phone_number", phone}
            };

            var token = Auth.AuthPost(email, password).Token_Data.token;
            Profile.ProfilePost(token, parameters);
            var actualPhone = Self.SelfGet(token).Client_Profile.phone_number;
            Assert.AreEqual(phone, actualPhone);

        }
        [TestCase("cevoni6781@settags.com", "n49bpxeCYG$A!Lb1", "n49bpxeCYG$A!Lb", "https://api.newbookmodels.com/api/v1/password/change/")]
        public void ChangePassword(string email, string password, string newPass, string url)
        {
            var parameters = new Dictionary<string, object>
            {
               {"old_password", password },
                {"new_password", newPass}
            };

            var token = Auth.AuthPost(email, password).Token_Data.token;
            Profile.ProfilePost(token, parameters);
            var actualPass = Self.SelfGet(token).Client_Profile.has_password;
        }
    }
}